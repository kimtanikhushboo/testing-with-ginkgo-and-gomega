package main_test

import (
	"bytes"
	"github.com/onsi/ginkgo/v2"
	"github.com/onsi/gomega"
	"io"
	"net/http"
)

var _ = ginkgo.Describe("Car service test", func() {
	ginkgo.Context("When successfully creating a new car", func() {
		ginkgo.It("should return the correct response", func() {
			requestBody := `{"id": "701","title": "GM","color": "Transparent"}`
			request, _ := http.NewRequest("POST", "http://localhost:8080/cars", bytes.NewBuffer([]byte(requestBody)))
			request.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(request)
			gomega.Expect(err).NotTo(gomega.HaveOccurred())
			gomega.Expect(response.StatusCode).To(gomega.Equal(http.StatusCreated))
		})
	})

	ginkgo.Context("When creating an invalid car", func() {
		ginkgo.It("should return an error in the response", func() {
			requestBody := ""
			request, _ := http.NewRequest("POST", "http://localhost:8080/cars", bytes.NewBuffer([]byte(requestBody)))
			request.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			response, err := client.Do(request)
			gomega.Expect(err).NotTo(gomega.HaveOccurred())
			gomega.Expect(response.StatusCode).To(gomega.Equal(http.StatusBadRequest))

			responseBody, _ := io.ReadAll(response.Body)
			gomega.Expect(string(responseBody)).To(gomega.ContainSubstring("Failed"))
		})
	})
})
