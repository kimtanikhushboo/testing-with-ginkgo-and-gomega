# What is Ginkgo

Ginkgo is a testing framework for Go programming language, which is designed to facilitate writing behavior-driven (BDD) tests. It provides a simple and intuitive syntax for writing test suites, cases and assertions, making it easier to write, read and maintain tests.

One of the main benefits of Ginkgo is that it provides an expressive and flexible way to write tests that can be easily understood by both technical and non-technical team members. It uses a natural language format that allows developers to write tests in a way that closely resembles the way users interact with the system. This makes it easier to understand and maintain tests, and also helps ensure that the tests are aligned with the expected behavior of the system.

In the context of microservices testing, Ginkgo is particularly well-suited for testing individual services as well as the interactions between them. It provides a flexible and expressive syntax for testing both positive and negative scenarios, such as testing the behavior of the service when receiving invalid inputs or testing the resilience of the service to network failures.

# What is Gomega

Gomega is a matcher/assertion library. It is best paired with the Ginkgo BDD test framework, but can be adapted for use in other contexts too.
Gomega provides a rich set of matchers that allow you to easily and expressively test your code for various conditions and outcomes.

Matchers in Gomega are functions that return a boolean value indicating whether a given assertion is true or false. For example, the Equal matcher checks whether two values are equal, and returns true if they are, and false if they are not.

# Getting Started
## Set up a new Ginkgo test suite
### Bootstrapping a Suite

Say you have a package named **cars** that you'd like to add a Ginkgo suite to. To bootstrap the suite run:
 ```
 cd path/to/cars 
 ginkgo bootstrap
 
 ```
First, ginkgo bootstrap generates a new test file and places it in the cars_test package. 

*Go code is organized into modules. A module is usually associated with a repository and consists of a series of versioned packages. Most of the times each package is associated with a single directory within the module’s file tree containing a series of source code files. When testing Go code, unit tests for a package resides within the same directory as the package and are named *_test.go. Ginkgo follows this convention.***

### modulename_suite_test.go file 
Next we define a single testing test: func TestCars(t *testing.T). This is the entry point for Ginkgo - the go test runner will run this function when you run go test or ginkgo

RegisterFailHandler(Fail) is the single line of glue code connecting Ginkgo to Gomega. If we were to avoid dot-imports this would read as gomega.RegisterFailHandler(ginkgo.Fail) - what we're doing here is telling our matcher library (Gomega) which function to call (Ginkgo's Fail) in the event a failure is detected.

Finally the RunSpecs() call tells Ginkgo to start the test suite, passing it the *testing.T instance and a description of the suite. You should only ever call RunSpecs once and you can let Ginkgo worry about calling *testing.T for you.

## Add Test Case (specs)

In Ginkgo, specs are the individual test cases that make up a test suite. A spec typically consists of a description of the behavior being tested, followed by the actual test code.

Create New Specs
```
cd path/to/module-to-be-tested 
ginkgo generate testname
```
This will create a testname_test.go file in the path

## BDD Directives
- **Describe** This is the top-level component of a Ginkgo test suite, and is used to group together a set of related tests. A Describe block typically describes a specific aspect of the system being tested, such as a package or module.
    
- **Context** Within a Describe block, you can use context blocks to further group related tests together. A Context block typically describes a specific set of conditions or inputs for the tests being run.

- **It** within a Context block, you can define individual test cases using It blocks. An It block describes a specific behavior or feature of the system being tested and contains the actual test code.

## Run your tests

```
cd path/to/module
ginkgo
```
For Further Exploration Refer: https://onsi.github.io/ginkgo